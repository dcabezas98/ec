## Estructura de Computadores: Preguntas Práctica 1

##### David Cabezas

### Sesión de depuración saludo.s

1.

Con `list`, localizamos la línea que contiene la orden `mov
longsaludo, %edx`, colocamos un `breakpoint` en dicha línea y lanzamos
la ejecución con `run`. Ejecutamos la instrucción con `stepi` y
procedemos a consultar el valor del registro con `print %edx`.

El valor de `EDX` tras ejecutar `mov longsaludo, %edx` es 28 (en
decimal) o 0x1c. Se trata de la longitud de la cadena saludo,
que contiene el valor *"Hola a todos!\nHello, World!\n"*.

Coincide con los valores obtenidos al ejecutar la orden `info
registers`.

2.

De la misma forma que antes, hemos obtenido que el valor del registro
`ECX` tras ejecutar `mov $saludo, %ecx` es 134516887 (= 0x8049097).
Este valor corresponde a la dirección de memoria de la variable *saludo*.

3.

Cambiamos `saludo.s` y volvemos a ensamplarlo y enlazarlo. Depuramos
el nuevo ejecutable y obtenemos que el nuevo valor de `ECX` tras
ejecutar `mov saludo, $ecx` es 1634496328 (= 0x616c6f48).

Esto se debe a que transforma los caracteres del string a su valor en
ASCII en hexadecimal hasta completar los 32 bits.

4.

Cada caracter de la variable *saludo* ocupa un byte. Podemos comprobar
con `x/32cb &saludo` que se almacena cada uno en una dirección de
memoria, por lo que *saludo* ocupa 28 direcciones de memoria (tiene 28
caracteres). Y justo a continuación, se almacena un byte para
*longsaludo*, que podemos comprobar con `x/4cb &longsaludo`.
Por lo que la sección de datos del programa ocupa 29 bytes.

5.

Volcado como entero hexadecimal:

```
	(gdb) x /1xb &longsaludo
	0x80490b3:	0x1c
```

Volcado como 4 bytes hex:

	(gdb) x /4xb &longsaludo
	0x80490b3:	0x1c	0x00	0x00	0x00

*longsaludo* ocupa la dirección 0x80490b3. El byte en la primera posición
es *00011100*, el menos significativo, por lo que podemos deducir que los
procesadores de la línea *x86* usan el criterio del extremo menor
(little-endian), en el que los bits más significativos son los de la 
derecha.

6.

Colocando un `breakpoint` en la lína correspondiente y con la orden 
`disassemble` podemos ver que la instrucción `mov $1, %ebx` ocupa 5 
posiciones de memoria, desde la 0x08048079 hasta la 0x0804807d, ya 
que la siguiente instrucción empieza en la 0x0804807e.

```
 => 0x08048079 <+5>:	mov    $0x1,%ebx
	0x0804807e <+10>:	mov    $0x8049097,%ecx
```

7.

Eliminando la primera instrucción `int 0x80` el programa finaliza
normalmente pero no imprime el resultado en pantalla.

```
	(gdb) run
	Starting program: /home/dcabezas/dgiim/ec/practica1/saludo 
	[Inferior 1 (process 3982) exited normally]
```

En cambio, eliminando la segunda instrucción `int 0x80` el programa
imprime el resultado en pantalla, pero produce un *Segmentation fault*
en lugar de salir normalmente.

```
	(gdb) run
	Starting program: /home/dcabezas/dgiim/ec/practica1/saludo 
	Hola a todos!
	Hello, World!

	Program received signal SIGSEGV, Segmentation fault.
	0x08048096 in ?? ()
```

8.

El número de llamada del sistema ***READ*** en el kernel Linux 32bits es
el 3. Esto puede comprobarse con `cat /usr/include/asm/unistd_32.h`,
donde podemos encontrar la siguiente línea:

>#define __NR_read 3


### Sesión de depuración suma.s

1.

`EAX` contiene el valor 37 (=0x25) justo antes de ejecutar `RET`.
Lo he comprobado con un `break` en la línea correspondiente y la orden
`info reg`. Pero puedo razonarse a partir de que es la suma de todos
los elementos de la lista (1, 2, 10, 1, 2, 0b10, 1, 2, 0x10). Donde 0b10
es 2 en decimal y 0x10 es 16.

`(.-lista)` corresponde al número de bytes que ocupa lista, como cada entero
ocupa 4B (32b) y tiene 9 enteros, `(.-lista)` tiene el valor 36.
`(.-lista)/4` tiene el valor 9 (longitud de la lista), podemos comprobarlo
con un `print` de la variable *longlista*.

2.

Se obtiene el valor -3 (=0xfffffffd), esto se debe a que 0xffffffff corresponde 
al valor -1 (está en complemento a 2) y lo sumamos 3 veces. 

3.

En `ddd`, pasando el cursor sobre una variable podemos ver su dirección de memoria 
asignada, lo hacemos con las etiquetas de funciones suma y bucle.

0x8048095 <suma>
0x80480a0 <bucle>

4.

`EIP` contine una dirección de memoria de instrucción, que va avanzando 
a medida que el procesador va ejecutando el programa.
`ESP` es el puntero de pila.

5.

Antes de ejecutar `CALL`, `ESP` contiene la dirección 0xffffd0e0.
Antes de ejecutar `RET`, `ESP` contiene la dirección 0xffffd0dc.

La diferencia entre ambos valores es 4, el tamaño de pila usado para
almacenar la dirección de retorno de la función. Este tamaño es necesario
entre la llamada y el retorno de la función (entre `CALL` y `RET`).

6.

La instrucción `CALL` modifica los registros:
`EAX`, donde guarda el resultado de las sumas.
`ESP`, donde almacena la dirección de retorno para `RET`.
`EDX`, que actúa como contador.
`EIP`, que almacena direcciones de las instrucciones a ejecutar.

7.

La instrucción `RET` modifica los registros:
`ESP`, porque vuelve a la dirección de retorno guardada en la pila.
`EIP`, su modificación es necesaria para el avance del programa.

8.

Colocando diversos breakpoints y usando volcados de memoria, he podido 
comprobar que primero apunta a la dirección 0xffffd020, que contiene el 
valor 0x00000001, al ejecutar `CALL`, apunta a 0xffffd01c, donde se 
guarda el valor 0x08048084. Tras `push %edx`, pasa a apuntar a 0xffffd018,
donde está el valor 0x00000000. Luego se ejecuta `pop %edx` y pasa a 
apuntar a 0xffffd01c de nuevo, con el valor 0x08048084. Y después de `RET`
apunta a 0xffffd020, con el valor 0x00000001.

9.

`mov $0, %edx` ocupa 5 posiciones de memoria, desde la 0x0804809b hasta la
0x080480a0. Puede verse en el desensamblado de *ddd* o utilizando `objdump`. Del mismo modo podemos ver que `inc %edx` ocupa únicamente la posición
0x080480a3. 

Códigos máquina (en *Machine Code Window* de *ddd* o con `objdump`):

> 0x0804809b <suma+6>:   ba 00 00 00 00       	mov    $0x0,%edx

> 0x080480a3 <bucle+3>:  42                   	inc    %edx

10.

Que el no se volvería de la subrutina al programa principal, por lo que 
el programa terminaría con un *Segmentation fault*.

```
	(gdb) run
	Starting program: /home/dcabezas/dgiim/ec/practica1/suma 

	Program received signal SIGSEGV, Segmentation fault.
	0x080480a9 in ?? ()
```










